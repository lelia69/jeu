import { Letter } from "./letter";
import { Player } from "./player";

const submitBtn = document.querySelector('#submitBtn')
const form = document.querySelector('#form')
const boardGame = document.querySelector('#boardGame')
const playGame = document.querySelector('#playGame')
let players = [];
let input = document.querySelector('#player1')
let playerName = document.querySelector('#playerName')
let btnPioche = document.querySelector('#btnPioche')
let playerEasel = document.querySelector('#playerEasel')
let nbrPioche = document.querySelector('#nbrPioche')
let easelli = document.querySelectorAll('.easelli')
let ulEasel = document.querySelector('#ulEasel')
let btnRefresh = document.querySelector('#btn2')
let tds = document.querySelectorAll('td')
let valider = document.querySelector('#btn1')

form.addEventListener('submit', (event) => {
    event.preventDefault();
    boardGame.style.display = 'block';
    playGame.style.display = 'block';
    form.style.display = 'none';
    addPlayer(input.value);
    displayPlayer();

});

/**
 * fonction qui permet d'ajouter un nouveau joueur au tableau
 * @param {string} name 
 */
function addPlayer(name) {
    players.push({ name: name })
    console.log(players)
}


/**
 * fonction qui permet d'afficher le nouveau joueur qui commence le jeu
 */
function displayPlayer() {

    for (let play of players) {
        let span = document.createElement('span');
        span.textContent = 'JOUEUR : ' + play.name;
        playerName.appendChild(span)

    }
}

/* tableau de toutes les lettres contenus dans la pioche */

let pioche = [
    new Letter('A', '1'),
    new Letter('A', '1'),
    new Letter('A', '1'),
    new Letter('A', '1'),
    new Letter('A', '1'),
    new Letter('A', '1'),
    new Letter('A', '1'),
    new Letter('A', '1'),
    new Letter('A', '1'),

    new Letter('B', '3'),
    new Letter('B', '3'),

    new Letter('C', '3'),
    new Letter('C', '3'),

    new Letter('D', '2'),
    new Letter('D', '2'),
    new Letter('D', '2'),

    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),
    new Letter('E', '1'),

    new Letter('F', '4'),
    new Letter('F', '4'),

    new Letter('G', '2'),
    new Letter('G', '2'),

    new Letter('H', '4'),
    new Letter('H', '4'),

    new Letter('I', '1'),
    new Letter('I', '1'),
    new Letter('I', '1'),
    new Letter('I', '1'),
    new Letter('I', '1'),
    new Letter('I', '1'),
    new Letter('I', '1'),
    new Letter('I', '1'),

    new Letter('J', '8'),

    new Letter('K', '10'),

    new Letter('L', '1'),
    new Letter('L', '1'),
    new Letter('L', '1'),
    new Letter('L', '1'),
    new Letter('L', '1'),

    new Letter('M', '2'),
    new Letter('M', '2'),
    new Letter('M', '2'),

    new Letter('N', '1'),
    new Letter('N', '1'),
    new Letter('N', '1'),
    new Letter('N', '1'),
    new Letter('N', '1'),
    new Letter('N', '1'),

    new Letter('O', '1'),
    new Letter('O', '1'),
    new Letter('O', '1'),
    new Letter('O', '1'),
    new Letter('O', '1'),
    new Letter('O', '1'),

    new Letter('P', '3'),
    new Letter('P', '3'),

    new Letter('Q', '8'),

    new Letter('R', '1'),
    new Letter('R', '1'),
    new Letter('R', '1'),
    new Letter('R', '1'),
    new Letter('R', '1'),
    new Letter('R', '1'),

    new Letter('S', '1'),
    new Letter('S', '1'),
    new Letter('S', '1'),
    new Letter('S', '1'),
    new Letter('S', '1'),
    new Letter('S', '1'),

    new Letter('T', '1'),
    new Letter('T', '1'),
    new Letter('T', '1'),
    new Letter('T', '1'),
    new Letter('T', '1'),
    new Letter('T', '1'),

    new Letter('U', '1'),
    new Letter('U', '1'),
    new Letter('U', '1'),
    new Letter('U', '1'),
    new Letter('U', '1'),
    new Letter('U', '1'),

    new Letter('V', '4'),
    new Letter('V', '4'),

    new Letter('W', '10'),

    new Letter('X', '10'),

    new Letter('Y', '10'),

    new Letter('Z', '10')]

console.log(pioche);


/**
 * Fonction qui permet de renvoyer une lettre aléatoirement issue de mon tableau Pioche puis la supprimer de Pioche
 * @param {array} tab 
 * @returns une valeur aléatoirement et la supprime de mon tableau
 */
function randel(tab) {

    let r = Math.round(Math.random() * (tab.length - 1));
    let rec = tab[r];
    tab.splice(r, 1);
    nbrPioche.innerHTML = pioche.length;
    return rec;

}

// au click, ma pioche délivre une nouvelle lettre et la place sur mon chevalet 
btnPioche.addEventListener('click', () => {
    ulEasel.appendChild(randel(pioche).toHTML())


});

/*  essai incrément score, en attente..
let score = 0;
for (let index = 0; index < tds.length; index++) {
    const element = tds[index];


}*/











