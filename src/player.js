import { Letter } from "./letter";


export class Player{
    pioche = [];

    addLetter(label){
        this.pioche.push(new Letter(label))
    }

    toHTML(){
        let newUl = document.createElement('ul')
        
        for(let lett of this.pioche){
            newUl.appendChild(lett.toHTML())
        }
        return newUl
    }


}


/*
newLi.addEventListener('dragstart',(event)=>{
    event.dataTransfer.setData("text/plain",newLi.textContent)
});
newLi.addEventListener('drop',(event)=>{
    newLi.textContent= event.dataTransfer.getData("text/plain")
}) */