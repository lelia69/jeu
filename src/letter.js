export class Letter {
    constructor(label, value) {
        this.label = label;
        this.value = value;

    }

    toHTML() {
        let newLi = document.createElement('li');
        newLi.textContent = this.label;
        newLi.classList.add('easelli')
        newLi.setAttribute('draggable', 'true');
        newLi.id = this.value
        newLi.addEventListener('dragend', (event) => {
            let newCase = document.elementFromPoint(event.clientX, event.clientY);
            newCase.appendChild(newLi)
            newLi.style.margin = "0px 0px 0px 0px";
            newLi.style.width = "40px";
            newLi.style.height = "34px"
            let uleasel = document.querySelector('#ulEasel')
            uleasel.style.boxShadow = "none";



        })
        return newLi


    }



}


