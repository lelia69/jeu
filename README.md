Au cours de ma formation 'Developpeur Web/ developpeur Mobile' , nous devions, en guise de troisième projet, créer un Jeu en Javascript.
Le thème était au choix, en revanche, les fonctions obligatoires étaient les suivantes :

-Utiliser une ou deux Class JS.
- Avoir des données dans le JS qui seront modifiées en cours de jeu (barre de vie, score, progression, etc.)


Pour réaliser ce projet je me suis aidée des maquettes ci dessous : 

https://whimsical.com/jeu-js-start-5UjzEATo1G8DxvRUvhPYvz
https://whimsical.com/jeu-js-R4UKckV3fmekp12i4FWoB9
https://whimsical.com/jeu-js-user-case-RgCNjhGa25MMffw5BiyLr9

